<?php
namespace TMCms\Modules\SwipeLv;

use TMCms\Admin\Messages;
use TMCms\HTML\BreadCrumbs;
use TMCms\HTML\Cms\CmsFormHelper;
use TMCms\HTML\Cms\CmsTable;
use TMCms\HTML\Cms\Column\ColumnAccept;
use TMCms\HTML\Cms\Column\ColumnData;
use TMCms\HTML\Cms\Column\ColumnDelete;
use TMCms\HTML\Cms\Column\ColumnDone;
use TMCms\Log\App;
use TMCms\Modules\Settings\ModuleSettings;

class CmsSwipeLv
{
    public function _default()
    {
        BreadCrumbs::getInstance()
            ->addCrumb(__('WebHooks'), '?p=' . P)
            ->addAction(__('Add Item'), '?p=' . P . '&do=add')
        ;

        $is_test = !! \TMCms\Modules\Settings\ModuleSettings::getCustomSettingValue('swipe_lv', 'is_test');
        $res = json_decode(ModuleSwipeLv::apiRequest('webhooks'));
        $data = [];
        foreach($res->items as $item_id){
            $item_res = json_decode(ModuleSwipeLv::apiRequest("webhooks/$item_id"), true);
            $item_res['production'] = !$item_res['is_test'];
            if($is_test != $item_res['is_test'])
                continue;
            unset($item_res['errors']);
            $data[] = $item_res;
        }
//        dump($data);

        echo CmsTable::getInstance()
            ->addData($data)
            ->addColumn(ColumnData::getInstance('event')
                ->enableTranslationColumn()
//                ->href('?p=' . P . '&do=edit&id={%id%}')
                ->enableOrderableColumn()
            )
            ->addColumn(ColumnData::getInstance('url')
                ->enableTranslationColumn()
//                ->href('?p=' . P . '&do=edit&id={%id%}')
                ->enableOrderableColumn()
            )
            ->addColumn(ColumnDone::getInstance('production'))
            ->addColumn(ColumnDelete::getInstance())
        ;
    }

    public function add()
    {
        BreadCrumbs::getInstance()
            ->addCrumb(__('WebHooks'), '?p=' . P)
            ->addCrumb('Add New WebHook');
        echo $this->__add_edit_form();
    }


    private function __add_edit_form($data = NULL)
    {
        $edit_mode = true;
        if (!$data) {
            $data = [];
            $edit_mode = false;
        }
        $fields = [ // Field to generate (overwrites "combine" auto-generated fields)
            'event' => [
                'title'=> 'Event',
                'type' => 'select',
                'options' => [''=>'--- choose an event ---','payment.created'=>'payment.created', 'payment.paid'=>'payment.paid', 'payment.failed'=>'payment.failed', 'invoice.expired'=>'invoice.expired', 'transfer.withdrawn'=>'transfer.withdrawn'],
                'validate' => [
                    'require', // Required to fill in any text
                ]
            ],
            'url' => [ // Field that required extra params
                'backup' => true, // Show recover panel
                'required' => true,
            ],
        ];

        // No params are required in the form, you may remove any
        return CmsFormHelper::outputForm('', [
            'data' => $data, // Can be simple key-value array or Entity
            'action' => '?p=' . P . '&do=_'.($edit_mode ? 'edit&id='. $data['id'] : 'add'), // Url to submit post data, may be omitted for auto "_function" prefix
            'button' => $edit_mode ? 'Update WebHook' : 'Add WebHook', // Text on submit button
            'cancel' => true, // Show Cancel button

            'fields' => $fields,
        ]);
    }

    public function _add()
    {
        ModuleSwipeLv::apiRequest("webhooks", "POST", ['event' => $_POST['event'],'url' => $_POST['url'],]);

        Messages::sendMessage("Swipe.lv WebHook ".$_POST['event']." added");

        go('?p='.P);
    }

    public function _delete()
    {
        if(empty($_GET['id']))
            back();
        $id = $_GET['id'];

        ModuleSwipeLv::apiRequest("webhooks/$id", "DELETE");

        Messages::sendMessage("Swipe.lv WebHook $id deleted");
        App::add("Swipe.lv WebHook $id deleted");

        back();
    }

    public function settings()
    {
        echo ModuleSettings::requireTableForExternalModule();
    }

    public function _settings()
    {
        ModuleSettings::requireUpdateModuleSettings();

        if (IS_AJAX_REQUEST) {
            die('1');
        }
        back();
    }


}
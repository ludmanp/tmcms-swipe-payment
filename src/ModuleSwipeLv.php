<?php
namespace TMCms\Modules\SwipeLv;

use TMCms\Log\FrontendLogger;
use TMCms\Modules\IModule;
use TMCms\Traits\singletonInstanceTrait;
use \TMCms\Modules\Settings\ModuleSettings;

defined('INC') or exit;

class ModuleSwipeLv  implements IModule
{
    use singletonInstanceTrait;

    static $languages = ['en','lv','lt','ru'];

    static public function apiRequest($url, $method = "GET", $params = ""){
        $url = trim($url, '/');
        $method = strtoupper($method);
        if(!in_array($method, ['GET', 'POST', 'PUT', 'DELETE']))
            // @todo throw exception
            return false;
        $public_key = ModuleSettings::getCustomSettingValue('swipe_lv', 'public_key');
        $private_key = ModuleSettings::getCustomSettingValue('swipe_lv', 'private_key');
        if(!$public_key) {
            dump('Public key for Swipe.lv not defined');
        }
        if(!$private_key) {
            dump('Private key for Swipe.lv not defined');
        }

//        FrontendLogger::getInstance()->log(SELF . ' : ' . $url . ' : ' . $public_key);
        $ver = ModuleSettings::getCustomSettingValue('swipe_lv', 'version');
        if(!$ver){
            $ver = '0.5';
        }

        if($ver == '0.6'){
            $authorization_header = "Bearer ".$private_key;
        }else {
            $timestamp = (string)time();
            if(is_array($params))
                $params = json_encode($params);

            $authorization_message = $timestamp." $method /api/v0.5/$url/ ".$params;
            $authorization = hash_hmac('sha256', $authorization_message, $private_key);
            $authorization_header = $public_key.','.$timestamp.','.$authorization;
        }


        $options = array(
            'http' => array(
                'header'  => "Content-type: application/json\r\nAuthorization: ".$authorization_header,
                'method'  => $method,
                'content' => $params,
                'ignore_errors' => true
            ),
        );
        if($ver=='0.6'){
            $base_url = "https://gate.decta.com/api/v0.6/";
        }else{
            $base_url = "https://swipe.lv/api/v0.5/";
        }

        $context = stream_context_create($options);
        $result = file_get_contents($base_url . $url."/", false, $context);
        return $result;
    }
}